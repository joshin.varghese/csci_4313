Setting up LAMP stack program which includes Raspbian (Linux, comes with the system), Apache web server, MySQL database, and PHP which is the script language.
First I updated all the packets by using the following commands:
	Sudo apt-get update
	Sudo apt-get upgrade
Then, I set up Apache using the following command:
	Sudo apt-get install apache2
I then typed in the IP address of the RPi , and it showed me the Apache2 default page, which said “it works!”.
Then, I set up PHP using the following command:
	Sudo apt-get install php5 libapache2-mod-php5
I then edited the phpinfo.php file so that the corresponding page shows up on GIU
Then, I setup MySQL using the following command:
	Sudo apt-get install phpMyAdmin
I then connected the phpMyAdmin with Apache web server, and restarted the apache web server.
With this my web server is set, and I am able to administer my database in the web-browser itself.

All ports from incoming traffic is open by default, and can be seen using the following command:
	Sudo iptables -L
Here, we can see that all the three standard chains are set to ACCEPT, and no rules exist.

Now to setup Basic Firewall, and just keep ports 22 and 80 open, I first installed ufw by using the following command:
	Sudo apt-get install ufw
I then opened port 22 and 80 to be accessed by everyone by using the following command:
	Sudo ufw allow 22
	Sudo ufw allow 80
After enabling ufw, and then check the ufw status using the following command, I was able to see the rules, that port 22 and 80 are allowed anywhere.
	Sudo ufw status

To change the name of the Raspberry Pi, I used the following command:
	Sudo nano /etc/hostname
I then changed the name that appeared, then saved it, and rebooted it, and it changed the name.

Next, to create a new user with group privileges, I first created a username for the user using the following command:
	Sudo adduser Joshin

I then gave the user sudo privileges with the following privileges by using the following command:
	Sudo visudo
Next to change the password, I used the following commands to first enter my current password and then enter and reenter my new password.
	Passwd
To reboot the server, I only had to use the following command:
	Sudo reboot
Next, I wrote a python script to reboot the server, I used the following command to create the python script
	Sudo nano reboot.py
In the editor I wrote the following code:
	command = "/usr/bin/sudo /sbin/reboot"  
	import subprocess  
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)  
	output = process.communicate()[0]  
	print output  
	import time  
	ts = time.time()  
	import datetime  
	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')  
	import time  
	ts = time.time()  
	import datetime  
	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')  
	import time  
	ts = time.time()  
	import datetime  
	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')  
Int his I used subprocess command to run allow me to run system command, and added timestamps to log the date and time.
I then executed the script from the webpage with the following commands:
<?php  
 exec("python /var/www/remotecontrol/python/reboot.py");  
?>  


